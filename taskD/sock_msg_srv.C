#include "sock.h"
#include <sys/select.h>

#define MSG1 "Invalid command for server" 

// process a client's commands 
int process_cmd (int fd )
{
    char    buf[BUFSIZE];
    time_t  tim;
    char*   cptr;

    
    char filename[BUFSIZE];
    int pos=-1, len=-1; 
    off_t offset=-1;
    char rbuf[BUFSIZE];//BUFSIZE
    // read command from a client until QUIT_CMD 
    while (read(fd, buf, sizeof(buf)) > 0) 
    {
       int cmd = QUIT_CMD;
       (void)sscanf(buf,"%d",&cmd);
       switch (cmd) {
          case READ_CMD:{
            (void)sscanf(buf,"%d %s %d %d",&cmd, filename, &pos, &len);
            if(pos==-1 || len==-1){
              // 1st server answer
              sprintf(rbuf, "[ERROR] bad input args %d %s %d %d", cmd, filename, pos, len);
              write(fd, rbuf, BUFSIZE);
              break;
            }
            int rfd;
            rfd = open(filename, O_RDONLY);
            offset = lseek(rfd,(off_t)pos, SEEK_SET);                     
            if(offset!=(off_t)-1){
              ssize_t rc =  read(rfd, rbuf, (size_t)len);
              rbuf[rc]='\0';
             if(rc==-1)
                sprintf(rbuf,"%s",strerror(errno));
              offset =-1;
              close(rfd);
              write(fd, rbuf, BUFSIZE);
              break;
            }                        
            sprintf(rbuf,"%s",strerror(errno));
            offset =-1;
            close(rfd);
            // 1nd server answer: error because of lseek()
            write(fd, rbuf, BUFSIZE);
            break;
          }
          case WRITE_CMD:{
            (void)sscanf(buf,"%d %s %d %d",&cmd, filename, &pos, &len);
            if(pos==-1 || len==-1){
              // 1st server answer
              strcpy(rbuf,"[ERROR] bad input args");
              write(fd, rbuf, BUFSIZE);
              break;
            }
            int wfd=-1;
            wfd = open(filename, O_WRONLY | O_CREAT, 0666);
            offset = lseek(wfd,(off_t)pos, SEEK_SET);
            
            if(offset!=(off_t)-1){
              // 1st server answer: wait for text from client
              strcpy(rbuf,"ready for writing");
              write(fd, rbuf, BUFSIZE);
              read(fd, buf, BUFSIZE);
        
              ssize_t rc =  write(wfd, buf, (size_t)len);
              if(rc==-1)
                sprintf(rbuf,"[ERROR] %s",strerror(errno));
              else
                strcpy(rbuf,"writing is completed");
              offset =-1;
              int cl = close(wfd);

              // 2st server answer: ok or error while writing
              write(fd, rbuf, BUFSIZE);
              break;
            }
            sprintf(rbuf,"[ERROR] %s",strerror(errno));
            offset =-1;
            close(wfd);
            // 1st server answer: error because of lseek()
            write(fd, rbuf, BUFSIZE);
            break;
          }
          case QUIT_CMD:
             return cmd;

          default:
            // 1st server answer: nonono
             write(fd,MSG1, sizeof(MSG1)+1);
       }
    }
    return 0;
}

int main( int argc, char* argv[]) 
{
   char  buf[80], socknm[80];
   int  port=-1, nsid, rc;
   fd_set  select_set;
   struct timeval timeRec;

   if (argc < 2) {
      std::cerr << "usage: " << argv[0] << " <port> <host>\n";
      return 1;
   }

   // check if port no. of a socket name is specified 
   (void)sscanf(argv[1],"%d",&port);
   if (port==-1){
    std::cerr << "incorrect port\n";
    return 1;
   }

   // create a stream socket 
   sock sp( AF_INET, SOCK_STREAM );
   if (!sp.good()) return 1;
   // Bind a name to the server socket 
   if (sp.bind(argv[2],port) < 0) return 2;
   for (;;)  // Poll for client connections
   {
       timeRec.tv_sec = 1;       // polling time-out after one second 
       timeRec.tv_usec= 0;   
       FD_ZERO( &select_set );
       FD_SET( sp.fd(), &select_set );
       
        // wait for time-out or a read event occurs for socket
       rc = select(FD_SETSIZE, &select_set, 0, 0, &timeRec );
       if (rc > 0 && FD_ISSET(sp.fd(), &select_set)) 
       {
          // accept a connection request from a client socket
          if ((nsid = sp.accept(0, 0)) < 0) return 1;

          // process commands 
          if (process_cmd(nsid)==QUIT_CMD) break;

          close(nsid); // re-cycle file descriptor 
       }
       // do nothing but something doind is possible
    }
    sp.shutdown();
    return 0;
}
