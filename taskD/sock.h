#ifndef SOCK_H
#define SOCK_H

#include <iostream>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/sysinfo.h>

const int BACKLOG_NUM = 5;
#define BUFSIZE 2048
typedef enum { READ_CMD, WRITE_CMD, QUIT_CMD } CMDS;

class sock {
  private:
    int sid;// socket descriptor
    int domain;// socket domain
    int socktype;// socket type
    int rc;// status code

  	// Build a socket name based on the given hostname and port number 
    int constr_name( struct sockaddr_in& addr, const char* hostnm, int port ){
      addr.sin_family = domain;
      if (!hostnm) 
      	addr.sin_addr.s_addr = INADDR_ANY;
      else {
      	struct hostent *hp = gethostbyname(hostnm);
      	if (hp==0) {
          	perror("gethostbyname"); return -1;
      	}
        	memcpy((char*)&addr.sin_addr,(char*)hp->h_addr, hp->h_length);
      }
      addr.sin_port = htons(port);
      return sizeof(addr);
    };

  public:
    sock( int dom, int type, int protocol=0 ) : domain(dom), socktype(type){
      if ((sid=socket(domain=dom, socktype=type,protocol))<0)
        perror("socket");
    };

    ~sock(){ 
      shutdown();
      close(sid);
    };

    int fd() { return sid; };

    int good()    { return sid >= 0;  };

    int bind( const char* name, int port=-1 ) {
      struct sockaddr_in addr;
      unsigned int len = constr_name( addr, name, port);
      if ((rc= ::bind(sid, (struct sockaddr *)&addr, len))<0  ||
          (rc=getsockname(sid, (struct sockaddr*)&addr, &len))<0)
            perror("bind or getsockname");
      else std::cerr << "Socket port: " << ntohs(addr.sin_port) << std::endl;
      if (rc!=-1 && socktype!=SOCK_DGRAM && (rc=listen(sid,BACKLOG_NUM)) < 0) 
        perror("listen");
      return rc;
    };

    int accept ( char* name, int* port_p){
      if (!name) return ::accept(sid, 0, 0);
      struct sockaddr_in addr;
      unsigned int size = sizeof (addr);
      if ((rc = ::accept( sid, (struct sockaddr*)&addr, &size)) >-1){
        if (port_p) 
          *port_p = ntohs(addr.sin_port);
      }
      return rc;
    };
    int connect( const char* hostnm, int port=-1 ){
      struct sockaddr_in addr;
      int len = constr_name( addr, hostnm, port);
      if ((rc= ::connect(sid,(struct sockaddr *)&addr,len))<0)
        perror("bind");
      return rc;
    };
    int write( const char* buf, int len, int flag=0, int nsid=-1 ){
      return ::send(nsid==-1 ? sid : nsid, buf, len, flag );
    };	
    int read( char* buf, int len, int flag=0, int nsid=-1 ){
      return ::recv(nsid==-1 ? sid : nsid, buf, len, flag );
    };

    int shutdown( int mode = 2 ){
      return ::shutdown (sid,mode);
    };
};// class sock

#endif
