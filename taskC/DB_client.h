#ifndef DB_CLIENT_H

#include <semaphore.h>
// #include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <stdbool.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>

#define DB_CLIENT_H
#define MAX_DB_SIZE 150
// database record data  --> Example: {Tula, 71}
struct rec_info{
  char city[256];
  unsigned int code;
};
typedef struct rec_info data;

// database record
struct record{
  // sem_t sem; // if a writer is changing record, a reader can't get access
  pthread_rwlock_t rwlock;
  pthread_rwlockattr_t attr;
  unsigned int key; // the way to check existing of the record when getting access
  data info; 
  struct record* prev;
  struct record* next;
};
typedef struct record db_record;
// shared memory structure
struct shmem{
  // if (change==1 && readers == 0) delete shared memory object
  sem_t editors; // only one writer has access 
  int readers; // ... and many readers
  bool key[MAX_DB_SIZE]; // the way to check existing of the record when getting access
  db_record* first;
  db_record* last;
  int len;
  db_record db_recs[MAX_DB_SIZE]; 
};
typedef struct shmem db;

const size_t shmem_size = sizeof(db);
const char* db_name = "/data_base";

class DB_client{
private:
  void* m_start;
  db* m_db;

  int check_existence(db_record* tmp_ptr, int rc, bool* unlock){
    *unlock = false;
    // int rc;
    // rc = pthread_rwlock_rdlock(&(tmp_ptr->rwlock));
    if(rc){
      printf("[ERROR]: pthread_rwlock_rdlock %d", rc);      
      if(rc == EINVAL)
        printf("[ERROR]: EINVAL - record deleted, cannot be read");
      return 1;
    } 

    if(!m_db->key[tmp_ptr->key]){
      printf("[ERROR]: record deleted, cannot be read"); fflush(0);
      rc = pthread_rwlock_unlock(&(tmp_ptr->rwlock));
      if(rc) printf("[ERROR]: pthread_rwlock_unlock");
      else *unlock = true;
      // destroy rwblock -->
      rc = pthread_rwlock_destroy(&(tmp_ptr->rwlock));
      if(rc){
        if(rc != EBUSY)// smb else is reading, that reader should destroy 
          printf("[ERROR]: rwlock_destroy");
      }
      else{
        rc = pthread_rwlockattr_destroy(&(tmp_ptr->attr)); 
        if(rc) printf("[ERROR]: rwlockattr_destroy");
      }
      // <-- destroy rwblock 
      return 2;  
    }// if(!m_db->key[tmp_ptr->key])
    return 0;
  }

public:  
  // common
  DB_client(void* st = NULL): m_start(st){;}
//--------------------------------------------------------------------------------------------------------------

  ~DB_client(){
    int rc = clean_db();
    if(munmap(m_start, shmem_size)<0) perror("munmap");
    if(!rc){
      printf("delete_db()\n");
      delete_db();
    }
  }
//--------------------------------------------------------------------------------------------------------------

  int clean_db(){
    int semval=-1;
    int rc; 
    if(sem_getvalue(&(m_db->editors), &semval)) perror("sem_getvalue");
    if(!(semval == 1 && m_db->readers == 0)){
      printf("NO clean_db()\n");
      return 1;
    }
    // delete db
    for(int i=0;i<MAX_DB_SIZE;i++){
      if(m_db->key[i]){
        rc = pthread_rwlock_unlock(&(m_db->db_recs[i].rwlock));
        if(rc) printf("[ERROR]: pthread_rwlock_unlock");
        rc = pthread_rwlock_destroy(&(m_db->db_recs[i].rwlock));
        if(rc)
            printf("[ERROR]: rwlock_destroy");
        else{
          rc = pthread_rwlockattr_destroy(&(m_db->db_recs[i].attr)); 
          if(rc) printf("[ERROR]: rwlockattr_destroy");
        }
      }// if(m_db->key[i])
    }
    // if(!semval) 
      // if( sem_post(&(m_db->editors)) ) perror("sem_post");
    // sem_getvalue(&(m_db->editors), &semval);
    // if(semval==1)
      if( sem_destroy(&(m_db->editors)) ) perror("sem_destroy");
    return 0;
  }

  void delete_db(){
    if(shm_unlink(db_name)) perror("shm_unlink");
  }
//--------------------------------------------------------------------------------------------------------------
  // first app
  sem_t* sem(){ return &(m_db->editors);}
  
  int create_db(){
    bool newDB = false;
    int rc;
    // if exists then open
    int db_fd = shm_open(db_name, O_RDWR, S_IRUSR | S_IWUSR);
    // if does not exist then create
    if (db_fd < 0) {
      newDB = true;
      db_fd = shm_open(db_name, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
      if (db_fd < 0){
        perror("Finished by shm_open");
        delete_db();
        return 1;
      }
      if (ftruncate(db_fd, shmem_size)< 0){
        perror("Finished by ftruncate");
        delete_db();
        return 1;
      }
    }
    char buf[50];

    if(!m_start)
      m_start = mmap(NULL, shmem_size, PROT_READ | PROT_WRITE, MAP_SHARED, db_fd, 0);
    else
      m_start = mmap(m_start, shmem_size, PROT_READ | PROT_WRITE, MAP_FIXED | MAP_SHARED, db_fd, 0);
    if (m_start == MAP_FAILED){
      perror("Finished by mmap");
      return 2;
    }
    else{
      m_db = (db*)m_start;
      if(newDB){
        printf("new DB\n");
        for(int i=0;i<MAX_DB_SIZE;i++)
          m_db->key[i] = false;
        m_db->first = NULL;
        m_db->last = NULL;
        m_db->len = 0;
        rc = sem_init( &(m_db->editors), 1, 1);
        if(rc<0) perror("sem_init");
        m_db->readers = 0;
      }// if(newDB)
    }
    
    printf("start: %p size: %zX\n", m_start, shmem_size);
    return 0;
  }
  void ins(data info,int pos=-1){
    db_record* tmp_ptr;
    int tmp_pos = 0;
    int phys_pos = -1;
    int rc;
    // search of empty space for a new record in m_db->db_recs
    for(int i=0;i<MAX_DB_SIZE;i++){
      if(m_db->key[i]==false){
        m_db->key[i]=true;
        phys_pos = i;
        printf("phys_pos: %i\n", phys_pos);
        break;
      }
    }
    m_db->db_recs[phys_pos].info = info;
    
    // rwlock init -->  
    rc = pthread_rwlockattr_init(&(m_db->db_recs[phys_pos].attr));
    if(rc) printf("[ERROR]: rwlockattr_init");
    rc = pthread_rwlockattr_setpshared(&(m_db->db_recs[phys_pos].attr), PTHREAD_PROCESS_SHARED);
    if(rc) printf("[ERROR]: rwlockattr_setpshared");
    rc = pthread_rwlock_init(&(m_db->db_recs[phys_pos].rwlock), &(m_db->db_recs[phys_pos].attr) );
    if(rc) printf("[ERROR]: rwlock_init");
    // <-- rwlock init 

    if(pos==-1) pos=0;
    if(pos>m_db->len) pos = m_db->len;
    //push front
    if(pos==0){
      //empty DB
      if(m_db->first == NULL){
        m_db->db_recs[phys_pos].next = NULL;
        m_db->last = &(m_db->db_recs[phys_pos]);
      }
      else{
        m_db->db_recs[phys_pos].next = m_db->first;
      }

      m_db->db_recs[phys_pos].prev = NULL;
      if(m_db->first){
        rc = pthread_rwlock_wrlock(&(m_db->db_recs[phys_pos].next->rwlock));
        if(rc) printf("[ERROR]: pthread_rwlock_wrlock");
        m_db->db_recs[phys_pos].next->prev = &(m_db->db_recs[phys_pos]);
        rc = pthread_rwlock_unlock(&(m_db->db_recs[phys_pos].next->rwlock));
        if(rc) printf("[ERROR]: pthread_rwlock_unlock");
      }
      m_db->first = &(m_db->db_recs[phys_pos]);
      printf("[INFO]: inserted city %s\tcode %i\n", m_db->first->info.city,m_db->first->info.code);
    }
    //push back
    else if(pos == m_db->len){
      m_db->db_recs[phys_pos].prev = m_db->last;
      m_db->db_recs[phys_pos].next = NULL;

      rc = pthread_rwlock_wrlock(&(m_db->db_recs[phys_pos].prev->rwlock));
      if(rc) printf("[ERROR]: pthread_rwlock_wrlock");
      m_db->db_recs[phys_pos].prev->next = &(m_db->db_recs[phys_pos]);
      rc = pthread_rwlock_unlock(&(m_db->db_recs[phys_pos].prev->rwlock));
      if(rc) printf("[ERROR]: pthread_rwlock_unlock");

      m_db->last = &(m_db->db_recs[phys_pos]);
      printf("[INFO]: inserted city: %s\tcode: %i\n", m_db->last->info.city,m_db->last->info.code);
    }
    // insert in pos
    else{
      tmp_ptr = m_db->first;
      while(tmp_pos != pos){
        tmp_ptr = tmp_ptr->next;
        tmp_pos += 1;
      }

      m_db->db_recs[phys_pos].prev = tmp_ptr;
      m_db->db_recs[phys_pos].next = tmp_ptr->next;

      rc = pthread_rwlock_wrlock(&(tmp_ptr->rwlock));
      if(rc) printf("[ERROR]: pthread_rwlock_wrlock");
      tmp_ptr->next = &(m_db->db_recs[phys_pos]);
      rc = pthread_rwlock_unlock(&(tmp_ptr->rwlock));
      if(rc) printf("[ERROR]: pthread_rwlock_unlock");

      rc = pthread_rwlock_wrlock(&(m_db->db_recs[phys_pos].next->rwlock));
      if(rc) printf("[ERROR]: pthread_rwlock_wrlock");      
      m_db->db_recs[phys_pos].next->prev = &(m_db->db_recs[phys_pos]);
      rc = pthread_rwlock_unlock(&(m_db->db_recs[phys_pos].next->rwlock));
      if(rc) printf("[ERROR]: pthread_rwlock_unlock");      

      printf("[INFO]: inserted city: %s\tcode: %i\n", m_db->db_recs[phys_pos].info.city,m_db->db_recs[phys_pos].info.code);
      // printf("[INFO]: Prev city: %s\tcode: %i\n", m_db->db_recs[phys_pos].prev->info.city,m_db->db_recs[phys_pos].prev->info.code);
      // printf("[INFO]: Next city: %s\tcode: %i\n", m_db->db_recs[phys_pos].next->info.city,m_db->db_recs[phys_pos].next->info.code);
    }
    m_db->db_recs[phys_pos].key = phys_pos;
    m_db->len += 1;
  }
//--------------------------------------------------------------------------------------------------------------
  int del(data info){
    if(!m_db->first){
      printf("[ERROR]: Database is empty!\n");
      return 1;
    }
    int todel = 0;
    db_record* tmp_ptr = m_db->first;
    int rc;
    char any_city[256];
    strcpy(any_city, "-");
    data curr_info;
    for(int tmp_pos=0;tmp_pos<m_db->len;tmp_pos++){
    
      if(!strcmp(info.city, any_city))
        strcpy(curr_info.city, tmp_ptr->info.city);
      else 
        strcpy(curr_info.city, info.city);
      if(info.code == 0) curr_info.code = tmp_ptr->info.code;
      else curr_info.code = info.code;

      //delete record
      if( !strcmp(curr_info.city, tmp_ptr->info.city) && curr_info.code == tmp_ptr->info.code){
        
        if(tmp_ptr != m_db->first){
          rc = pthread_rwlock_wrlock(&(tmp_ptr->prev->rwlock));
          if(rc) printf("[ERROR]: pthread_rwlock_wrlock");   
          tmp_ptr->prev->next = tmp_ptr->next;
          rc = pthread_rwlock_unlock(&(tmp_ptr->prev->rwlock));
          if(rc) printf("[ERROR]: pthread_rwlock_unlock");
        }
        else m_db->first = tmp_ptr->next;

        if(tmp_ptr != m_db->last){
          rc = pthread_rwlock_wrlock(&(tmp_ptr->next->rwlock));
          if(rc) printf("[ERROR]: pthread_rwlock_wrlock");  
          tmp_ptr->next->prev = tmp_ptr->prev;
          rc = pthread_rwlock_unlock(&(tmp_ptr->next->rwlock));
          if(rc) printf("[ERROR]: pthread_rwlock_unlock");
        }
        else m_db->last = tmp_ptr->prev; 

        printf("[INFO]: deleted city: %s code: %d\n",tmp_ptr->info.city,tmp_ptr->info.code);

        rc = pthread_rwlock_wrlock(&(tmp_ptr->rwlock));
        if(rc) printf("[ERROR]: pthread_rwlock_wrlock");
        m_db->key[tmp_ptr->key] = false;
        rc = pthread_rwlock_unlock(&(tmp_ptr->rwlock));
        if(rc) printf("[ERROR]: pthread_rwlock_unlock");        

        // destroy rwblock -->
        rc = pthread_rwlock_destroy(&(tmp_ptr->rwlock));
        if(rc){
          if(rc != EBUSY) // may be smb is already reading, so reader should destroy
            printf("[ERROR]: rwlock_destroy");
        }
        else{
          rc = pthread_rwlockattr_destroy(&(tmp_ptr->attr)); 
          if(rc) printf("[ERROR]: rwlockattr_destroy");
        }
        // <-- destroy rwblock 

        todel +=1;
        if(info.code != 0) break;
      }// if( !strcmp(curr_info.city, tmp_ptr->info.city) && curr_info.code == tmp_ptr->info.code)
      tmp_ptr = tmp_ptr->next;
    }// for(int tmp_pos;tmp_pos<db->len;tmp_pos++)
    m_db->len -= todel;
    return 0;
  }
//--------------------------------------------------------------------------------------------------------------
 
  int del(int pos=-1){
    if(!m_db->first){
      printf("[ERROR]: Database is empty!\n");
      return 1;
    }
    db_record* tmp_ptr = m_db->first;
    int tmp_pos = 0;
    int rc;
    if(pos < 0) pos = 0;
    if(pos >= m_db->len) pos = m_db->len-1;

    while(tmp_pos != pos){
      tmp_pos += 1;
      tmp_ptr = tmp_ptr->next;
    }

    if(tmp_ptr != m_db->first){
      rc = pthread_rwlock_wrlock(&(tmp_ptr->prev->rwlock));
      if(rc) printf("[ERROR]: pthread_rwlock_wrlock"); 
      tmp_ptr->prev->next = tmp_ptr->next;
      rc = pthread_rwlock_unlock(&(tmp_ptr->prev->rwlock));
      if(rc) printf("[ERROR]: pthread_rwlock_unlock");
    }
    else m_db->first = tmp_ptr->next;
    if(tmp_ptr != m_db->last){
      rc = pthread_rwlock_wrlock(&(tmp_ptr->next->rwlock));
      if(rc) printf("[ERROR]: pthread_rwlock_wrlock");  
      tmp_ptr->next->prev = tmp_ptr->prev;
      rc = pthread_rwlock_unlock(&(tmp_ptr->next->rwlock));
      if(rc) printf("[ERROR]: pthread_rwlock_unlock");
    }
    else m_db->last = tmp_ptr->prev; 

    rc = pthread_rwlock_wrlock(&(tmp_ptr->rwlock));
    if(rc) printf("[ERROR]: pthread_rwlock_wrlock");
    m_db->key[tmp_ptr->key] = false;
    rc = pthread_rwlock_unlock(&(tmp_ptr->rwlock));
    if(rc) printf("[ERROR]: pthread_rwlock_unlock");  

    // destroy rwblock -->
    rc = pthread_rwlock_destroy(&(tmp_ptr->rwlock));
    if(rc){
      if(rc != EBUSY)// may be smb is already reading, so reader should destroy
        printf("[ERROR]: rwlock_destroy");
    }
    else{
      rc = pthread_rwlockattr_destroy(&(tmp_ptr->attr)); 
      if(rc) printf("[ERROR]: rwlockattr_destroy");
    }
    // <-- destroy rwblock 

    m_db->len -= 1;
    return 0;
  }
//--------------------------------------------------------------------------------------------------------------

  void change(data newInfo, int pos=-1){
    if(pos<0 || m_db->first==NULL){
      printf("[WARNING]: create new record at the list head\n");
      ins(newInfo,0);
      return;
    }
    if(pos>=m_db->len){
     ins(newInfo,m_db->len);
     return;
    }

    int tmp_pos = 0;
    db_record* tmp_ptr = m_db->first;
    int rc;
    while(tmp_pos != pos){
      tmp_pos += 1;
      tmp_ptr = tmp_ptr->next;
    }
    rc = pthread_rwlock_wrlock(&(tmp_ptr->rwlock));
    if(rc) printf("[ERROR]: pthread_rwlock_wrlock");
    strcpy(tmp_ptr->info.city, newInfo.city);
    tmp_ptr->info.code = newInfo.code;
    rc = pthread_rwlock_unlock(&(tmp_ptr->rwlock));
    if(rc) printf("[ERROR]: pthread_rwlock_unlock");   
  }
//--------------------------------------------------------------------------------------------------------------

  int read_file(){    
    int fd = -1;
    int rc = -1;
    fd = open("DB_file", O_RDONLY, 0666);
    if(fd<0){
      perror("open DB_file");
      return 1;
    }
    ssize_t wsize = read(fd, m_db, shmem_size);
    if(wsize==(ssize_t)-1) perror("read");
    rc = close(fd);
    if(rc<0) perror("close");
    return rc;
  }
//--------------------------------------------------------------------------------------------------------------

  int write_file(){
    int fd = -1;
    int rc = -1;
    fd = open("DB_file", O_WRONLY|O_CREAT|O_TRUNC, 0666);
    if(fd<0){
      perror("open DB_file");
      return 1;
    }
    printf("start: %p size: %zX\n", m_start, shmem_size);
    ssize_t wsize = write(fd, m_start, shmem_size);
    if(wsize==(ssize_t)-1) perror("write");
    rc = close(fd);
    if(rc<0) perror("close");
    return rc;
  }
//--------------------------------------------------------------------------------------------------------------

  // second app
  int* reader(){ return &(m_db->readers);}
  void read_first(int n){
    int rc;
    int tmp_pos = 0;
    bool unlock = false;
    db_record* tmp_ptr = m_db->first;

    if(n > m_db->len) n = m_db->len;
    while(tmp_pos != n && tmp_ptr != NULL){
      rc = pthread_rwlock_rdlock(&(tmp_ptr->rwlock));
      if(check_existence(tmp_ptr,rc, &unlock)) break;
      printf("city: %s code: %d\n", tmp_ptr->info.city, tmp_ptr->info.code);

      if(!unlock){
        rc = pthread_rwlock_unlock(&(tmp_ptr->rwlock));
        if(rc) printf("[ERROR]: pthread_rwlock_unlock");  
      }
      else{ printf("%s\n", "unlocked!!!"); fflush(0);}

      tmp_pos += 1;
      tmp_ptr = tmp_ptr->next;
    }// while(tmp_pos != n || tmp_ptr != NULL)
  }
//--------------------------------------------------------------------------------------------------------------

  void read_last(int n){
    int rc;
    int tmp_pos = 0;
    db_record* tmp_ptr = m_db->last;
    bool unlock = false;

    if(n >= m_db->len) n = m_db->len;
    while(tmp_pos != n  && tmp_ptr != NULL){
      rc = pthread_rwlock_rdlock(&(tmp_ptr->rwlock));
      if(check_existence(tmp_ptr,rc, &unlock)) break;
      printf("city: %s code: %d\n", tmp_ptr->info.city, tmp_ptr->info.code);

      if(!unlock){
        rc = pthread_rwlock_unlock(&(tmp_ptr->rwlock));
        if(rc) printf("[ERROR]: pthread_rwlock_unlock");  
      }
      else{ printf("%s\n", "unlocked!!!"); fflush(0);}

      tmp_pos += 1;
      tmp_ptr = tmp_ptr->prev;
    }// while(tmp_pos != n || tmp_ptr != NULL)
  }
//--------------------------------------------------------------------------------------------------------------

  void read_rec(const char* city){
    int rc;
    int tmp_pos = 0;
    db_record* tmp_ptr = m_db->first;
    bool unlock = false;

    while(tmp_pos < m_db->len || tmp_ptr != NULL){
      rc = pthread_rwlock_rdlock(&(tmp_ptr->rwlock));
      if(check_existence(tmp_ptr,rc, &unlock)) break;
      if(!strcmp(tmp_ptr->info.city, city))
        printf("city: %s code: %d\n", tmp_ptr->info.city, tmp_ptr->info.code);

      if(!unlock){
        rc = pthread_rwlock_unlock(&(tmp_ptr->rwlock));
        if(rc) printf("[ERROR]: pthread_rwlock_unlock");  
      }
      else{ printf("%s\n", "unlocked!!!"); fflush(0);}
      
      tmp_pos += 1;
      tmp_ptr = tmp_ptr->next;
    } // while(tmp_pos < m_db->len || tmp_ptr != NULL)
  }

  void read_rec(unsigned int code){
    int rc;
    int tmp_pos = 0;
    bool unlock = false;
    bool found = false;
    db_record* tmp_ptr = m_db->first;

    while(tmp_pos < m_db->len || tmp_ptr != NULL){
      rc = pthread_rwlock_rdlock(&(tmp_ptr->rwlock));
      if(check_existence(tmp_ptr,rc, &unlock)) break;
     
      if(tmp_ptr->info.code == code){
        printf("city: %s code: %d\n", tmp_ptr->info.city, tmp_ptr->info.code);
        found = true;
      }

      if(!unlock){
        rc = pthread_rwlock_unlock(&(tmp_ptr->rwlock));
        if(rc) printf("[ERROR]: pthread_rwlock_unlock");  
      }
      else{ printf("%s\n", "unlocked!!!"); fflush(0);}

      if(found) break;
      
      tmp_pos += 1;
      tmp_ptr = tmp_ptr->next;
    }// while(tmp_pos < m_db->len || tmp_ptr != NULL)
  }    

};// class DB_client


#endif //DB_CLIENT_H